import { HttpException, Injectable } from '@nestjs/common';
import Reservation from '../entity/reservation';
import { ReservationSchema } from '../joi.schema';
import User from '../entity/user';
import { PAGE_SIZE, toIntegerOrZero } from './util';
import { FindOneOptions } from 'typeorm/find-options/FindOneOptions';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import GenericService from './generic.service';

@Injectable()
export default class ReservationService {
  constructor(private readonly g: GenericService) {}

  async getReservations({
    bikeId,
    page,
    userId,
  }: {
    bikeId: string;
    page: number;
    userId: string;
  }) {
    const whereClause: FindConditions<Reservation> = {
      status: 'ACTIVE',
    };
    if (bikeId) whereClause.bikeId = toIntegerOrZero(bikeId);
    if (userId) whereClause.userId = toIntegerOrZero(userId);

    const reservations = await Reservation.find({
      where: whereClause,
      take: PAGE_SIZE,
      relations: ['user', 'bike'],
      skip: (toIntegerOrZero(page) - 1) * PAGE_SIZE,
      order: { id: 'DESC' },
    });

    const totalReservationsCount = await Reservation.count({
      where: whereClause,
    });

    return {
      reservations,
      pageCount: Math.ceil(totalReservationsCount / PAGE_SIZE),
    };
  }

  async cancelReservation(rid: string, user: User) {
    if (user.isManager) throw new HttpException('Permission denied', 403);

    const res = await Reservation.findOne(rid);
    if (!res) throw new HttpException('Not Found', 404);
    if (res.userId !== user.id)
      throw new HttpException('Permission Denied', 403);
    if (res.status === 'INACTIVE')
      throw new HttpException('Reservation is already cancelled', 400);
    res.status = 'INACTIVE';
    await res.save();
    return {};
  }
}
