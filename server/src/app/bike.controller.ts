import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import ManagerGuard from '../manager.guard';
import AuthGuard from '../auth.guard';
import BikeService from './bike.service';
import Bike from '../entity/bike';
import { AUser } from './util';
import User from '../entity/user';
import Reservation from '../entity/reservation';

@Controller('/bike')
export default class BikeController {
  constructor(private readonly bs: BikeService) {}

  @UseGuards(ManagerGuard)
  @Post('')
  async addBike(@Body() param: Bike) {
    return this.bs.addBike(param);
  }

  @UseGuards(AuthGuard)
  @Get('')
  async getBikes(
    @Query()
    { page = '1', fromDate, toDate, model, location, color, rateAverage },
    @AUser() user: User,
  ) {
    return this.bs.getBikes(
      {
        page,
        fromDate,
        toDate,
        model,
        location,
        color,
        rateAverage,
      },
      user,
    );
  }

  @UseGuards(ManagerGuard)
  @Put('/:id')
  async updateBike(@Param('id') id: string, @Body() body: Bike) {
    return this.bs.updateBike(id, body);
  }

  @UseGuards(ManagerGuard)
  @Delete('/:id')
  async deleteBike(@Param('id') id: string) {
    return this.bs.deleteBike(id);
  }

  @UseGuards(AuthGuard)
  @Post('/:bikeId/review')
  async addRating(
    @Param('bikeId') bikeId: string,
    @Body() { rating }: { rating: number },
    @AUser() user,
  ) {
    console.log('Rating bike');
    return this.bs.addBikeRating(bikeId, rating, user);
  }

  @UseGuards(AuthGuard)
  @Post('/reserve')
  async reserveBike(@Body() body: Reservation, @AUser() user) {
    return this.bs.reserveBike(body, user);
  }
}
