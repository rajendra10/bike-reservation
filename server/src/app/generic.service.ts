import { HttpException, Injectable } from '@nestjs/common';
import User from '../entity/user';
import Bike from '../entity/bike';
import { toIntegerOrZero } from './util';

@Injectable()
export default class GenericService {
  async validateUser(userId: string | number) {
    const user = await User.findOne(toIntegerOrZero(userId));
    if (user) return user;
    if (!user) throw new HttpException('User Not found', 400);
  }
  async validateBike(bikeId: string | number) {
    const bike = await Bike.findOne(toIntegerOrZero(bikeId));
    if (bike) return bike;
    if (!bike) throw new HttpException('Bike Not found', 400);
  }

  async validateFromTo(from: string, to: string) {
    if (!from || !to)
      throw new HttpException(
        'From data and to date both should be present',
        400,
      );
    if (from >= to)
      throw new HttpException(
        'From datetime should be less than to datetime',
        400,
      );
  }
}
