import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import ManagerGuard from '../manager.guard';
import UserService from './user.service';
import User from '../entity/user';

@Controller('user')
export default class UserController {
  constructor(private readonly us: UserService) {}

  @Post('/login')
  async login(@Body() { email, password }) {
    return this.us.login({ email, password });
  }

  @Post('/signup')
  async signup(@Body() { email, password, name }) {
    return this.us.signup({ email, password, name });
  }

  @UseGuards(ManagerGuard)
  @Post('/add')
  async addUser(@Body() { email, password, name, isManager }: User) {
    return this.us.addUser({ email, password, name, isManager });
  }

  @UseGuards(ManagerGuard)
  @Get('/')
  async getUsers(@Query('page') page = '1') {
    return this.us.getUsers(page);
  }

  @UseGuards(ManagerGuard)
  @Put('/:id')
  async updateUser(@Param('id') id: string, @Body() body) {
    return this.us.updateUser(id, body);
  }

  @UseGuards(ManagerGuard)
  @Delete('/:id')
  async deleteUser(@Param('id') id: string) {
    return this.us.deleteUser(id);
  }
}
