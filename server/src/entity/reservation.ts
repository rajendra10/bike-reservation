import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import User from './user';
import Bike from './bike';

@Entity({ name: 'reservation' })
export default class Reservation extends BaseEntity {
  @PrimaryGeneratedColumn() id: number;
  @Column() userId: number;
  @Column() bikeId: number;
  @Column() fromDate: string;
  @Column() toDate: string;
  @Column({ default: 'ACTIVE' }) status: 'ACTIVE' | 'INACTIVE';

  @ManyToOne(() => User)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => Bike)
  @JoinColumn({ name: 'bikeId', referencedColumnName: 'id' })
  bike: Bike;
}
