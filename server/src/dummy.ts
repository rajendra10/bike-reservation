import User from './entity/user';
import Bike from './entity/bike';
import * as faker from 'faker';
import * as Bcryptjs from 'bcryptjs';
import Reservation from './entity/reservation';
import * as moment from 'moment';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import Rating from './entity/rating';
import * as _ from 'lodash';

const bootstrap = async () => {
  const app = await NestFactory.createApplicationContext(AppModule);
  await Reservation.delete({});
  await Rating.delete({});
  await User.delete({});
  await Bike.delete({});
  await seedUsers();
  await seedBikes();
  await seedReservations();
  await seedRating();
  await app.close();
};

const seedUsers = async () => {
  const users = [];
  for (let i = 0; i < 10; i++) {
    const u = new User();
    u.name = faker.name.findName();
    u.password = Bcryptjs.hashSync('1234', 10);
    u.isManager = i % 3 === 0;
    u.email = `hello${i}@gmail.com`;
    users.push(u);
  }
  await User.save(users);
};

const seedBikes = async () => {
  const bikes = [];
  for (let i = 0; i < 50; i++) {
    const bike = new Bike();
    bike.model = _.sample(['Model X', 'Model Y', 'Model Z']);
    bike.color = faker.commerce.color();
    bike.location = faker.address.city();
    bike.isAvailable = Math.random() < 0.7;
    bikes.push(bike);
  }
  await Bike.save(bikes);
};

const seedReservations = async () => {
  const users = await User.find({});
  const bikes = await Bike.find({});
  const reservations = [];
  for (let i = 0; i < 50; i++) {
    const randHour = parseInt(String(Math.random() * 200));
    const r = new Reservation();
    r.bikeId = _.sample(bikes).id;
    r.userId = _.sample(users).id;
    r.fromDate = moment().subtract(randHour, 'hour').toISOString();
    r.toDate = moment().add(randHour, 'hour').toISOString();
    r.status = Math.random() > 0.7 ? 'INACTIVE' : 'ACTIVE';
    reservations.push(r);
  }
  await Reservation.save(reservations);
};

const seedRating = async () => {
  const users = await User.find({});
  const bikes = await Bike.find({});
  const ratings = [];
  for (let i = 0; i < 100; i++) {
    const userId = _.sample(users).id;
    const bikeId = _.sample(bikes).id;
    let rating = await Rating.findOne({ where: { userId, bikeId } });
    if (!rating) {
      rating = new Rating();
      rating.userId = userId;
      rating.bikeId = bikeId;
      rating.rating = _.random(1, 5, true);
      ratings.push(rating);
    }
  }
  await Rating.save(ratings);
};

bootstrap().then();
