import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as path from 'path';
import UserController from './app/user.controller';
import UserService from './app/user.service';
import BikeService from './app/bike.service';
import BikeController from './app/bike.controller';
import Reservation from './entity/reservation';
import Rating from './entity/rating';
import Bike from './entity/bike';
import User from './entity/user';
import ReservationController from './app/reservation.controller';
import ReservationService from './app/reservation.service';
import GenericService from './app/generic.service';
const entitiesPath = path.join(__dirname, 'entity/*.entity.ts');
console.log({ entitiesPath });
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'data.db',
      entities: [User, Bike, Rating, Reservation],
      synchronize: true,
      logging: 'all',
    }),
  ],
  controllers: [BikeController, UserController, ReservationController],
  providers: [BikeService, UserService, ReservationService, GenericService],
})
export class AppModule {}
